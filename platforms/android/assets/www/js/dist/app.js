(function() {
  var app;

  app = angular.module('Application', ['ngRoute', 'ngResource']);

  app.config(function($routeProvider, $locationProvider) {
    return $routeProvider.when('/', {
      templateUrl: 'js/dist/views/home.html',
      controller: 'IndexController'
    }).when('/galerie', {
      templateUrl: 'js/dist/views/galerie.html',
      controller: 'IndexController'
    }).when('/about', {
      templateUrl: 'js/dist/views/about.html',
      controller: 'IndexController'
    }).when('/evenements', {
      templateUrl: 'js/dist/views/evenements.html',
      controller: 'IndexController'
    }).when('/evenement/:id', {
      templateUrl: 'js/dist/views/evenement.html',
      controller: 'IndexController'
    }).when('/video', {
      templateUrl: 'js/dist/views/video.html',
      controller: 'IndexController'
    }).otherwise({
      redirectTo: '/'
    });
  });

}).call(this);
